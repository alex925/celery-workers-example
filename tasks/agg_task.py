from celery.task import Task

# @current_app.task(
#     name='agg_task',
#     queue='processing-tasks.queue',
#     ignore_result=True
# )
# def agg_task(*args, **kwargs):
#     print(args, kwargs)


class AggTask(Task):
    name = 'agg_task'
    queue = 'aggregating-tasks.queue'

    def run(self, result_list: list):
        print(result_list)
