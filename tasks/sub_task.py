from celery.task import Task


class SubTask(Task):
    name = 'sub_task'
    queue = 'processing-tasks.queue'

    def run(self, result_list: list, *args, **kwargs):
        print('run sub_task')
        result_list.append(
            kwargs['a'] - kwargs['b']
        )
        return result_list

