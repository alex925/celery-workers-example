from celery.task import Task


class SumTask(Task):
    name = 'sum_task'
    queue = 'processing-tasks.queue'

    def run(self, result_list: list, *args, **kwargs):
        print('run sum_task')
        result_list.append(
            kwargs['a'] + kwargs['b']
        )
        return result_list


