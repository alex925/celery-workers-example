celery-workers
==============

Данный проект демонстрирует как работать с celery workers, которые обрабатывают
сообщения из rabbitmq очереди.


## Зависимости

- python 3.6
- rabbitmq


## Запуск проекта

Перед запуском проекта необходимо запустить rabbitmq:

```
docker run --rm \
           --name rabbit \
           -p "4369:4369" \
           -p "5672:5672" \
           -p "15672:15672" \
           -p "25672:25672" \
           -p "35197:35197" \
           -e "RABBITMQ_USE_LONGNAME=true" \
           rabbitmq:3.7.11-management
```

После этого, можно запустить celery workers:

```bash
./venv/bin/celery -A main:app worker -E -n processing-tasks@%h --queues processing-tasks.queue --loglevel info -c 1
./venv/bin/celery -A main:app worker -E -n aggregating-tasks@%h --queues aggregating-tasks.queue --loglevel info -c 1
```

Для того, чтобы передать задание для запущенных воркеров необходимо открыть
[панель управления rabbitmq](http://localhost:15672/#/queues), найти там 
очередь `worker-input.queue` и вставить в нее сообщение:

```json
{"sum": {"a": 1, "b": 2}, "sub": {"a": 1, "b": 1}}
```
