import json
import logging

from amqp import PreconditionFailed
from celery import Celery, bootsteps, chain
from kombu import Consumer, Exchange, Queue
from kombu.transport.pyamqp import Message as KombuPyAmqpMessage

from tasks.agg_task import AggTask
from tasks.sub_task import SubTask
from tasks.sum_task import SumTask

app = Celery(
    'simple_app',
    broker='pyamqp://guest:guest@127.0.0.1:5672//',
    include=['tasks.sum_task', 'tasks.sub_task', 'tasks.agg_task']
)

WORKER_INPUT_QUEUES = [
    Queue(
        "worker-input.queue",
        Exchange("worker.exchange"),
        routing_key="worker-input.queue"
    )
]

WORKER_INPUT_DEAD_LETTER_QUEUES = Queue(
        name="worker-input.dead-letter.queue",
        exchange=Exchange("worker-input.dead-letter.exchange"),
        routing_key="worker-input.dead-letter.key"
)

app.conf.task_queues = WORKER_INPUT_QUEUES + [
    Queue(
        'processing-tasks.queue',
        Exchange("worker.exchange"),
        'processing-tasks.queue'
    ),
    Queue(
        'aggregating-tasks.queue',
        Exchange("worker.exchange"),
        'aggregating-tasks.queue'
    ),
]

app.conf.task_routes = {
    'sum_task': {
        "queue": "processing-tasks.queue",
        "exchange": Exchange("worker.exchange"),
        "routing_key": "processing-tasks.queue"
    },
    'sub_task': {
        "queue": "processing-tasks.queue",
        "exchange": Exchange("worker.exchange"),
        "routing_key": "processing-tasks.queue"
    },
    'agg_task': {
        "queue": "aggregating-tasks.queue",
        "exchange": Exchange("worker.exchange"),
        "routing_key": "aggregating-tasks.queue"
    }
}


class DeclareDeadLetterQueues(bootsteps.StartStopStep):
    requires = {"celery.worker.components:Pool"}

    def start(self, worker):
        with worker.app.pool.acquire() as conn:
            try:
                WORKER_INPUT_DEAD_LETTER_QUEUES.bind(conn).declare()
            except PreconditionFailed:
                logging.info(
                    "Queue {} has already been declared with other "
                    "arguments".format(WORKER_INPUT_DEAD_LETTER_QUEUES.name)
                )
            else:
                logging.info(
                    "Created queue: '{}', exchange: '{}' "
                    "with key: '{}'".format(
                        WORKER_INPUT_DEAD_LETTER_QUEUES.name,
                        WORKER_INPUT_DEAD_LETTER_QUEUES.exchange.name,
                        WORKER_INPUT_DEAD_LETTER_QUEUES.routing_key
                    )
                )


class AppConsumer(bootsteps.ConsumerStep):
    def get_consumers(self, channel):
        return [Consumer(channel,
                         queues=WORKER_INPUT_QUEUES,
                         callbacks=[self.handle_message],
                         accept=["json"])]

    @staticmethod
    def handle_message(body, message: KombuPyAmqpMessage):
        logging.info('Start processing message')

        request = json.loads(body)

        result_list = []

        t1 = SumTask().si(result_list=result_list, **request['sum'])
        t2 = SubTask().s(**request['sub'])
        t3 = AggTask().s()
        tasks = [t1, t2, t3]
        chain(tasks)()

        message.ack()


app.steps["consumer"].add(AppConsumer)
app.steps["worker"].add(DeclareDeadLetterQueues)


if __name__ == '__main__':
    app.start()
